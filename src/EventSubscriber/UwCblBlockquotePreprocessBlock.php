<?php

namespace Drupal\uw_cbl_blockquote\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent;
use Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent;
use Drupal\uw_cfg_common\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwCblBlockquotePreprocessBlock.
 */
class UwCblBlockquotePreprocessBlock extends UwCblBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      BlockPreprocessEvent::name() => 'preprocessBlock',
    ];
  }

  /**
   * Preprocess blocks with blockquote and set variables.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\BlockPreprocessEvent $event
   *   The event.
   */
  public function preprocessBlock(BlockPreprocessEvent $event): void {

    // Check if we are on the right block to preprocess.
    if ($this->checkPreprocessBlock($event, 'uw_cbl_blockquote')) {

      // Get the variables from the event.
      $variables = $event->getVariables();

      // Load in the block
      $block = $variables->getByReference('content');

      // Set the variables for the blockquote.
      $blockquote = [
        'text' => $block['field_uw_bq_quote_text'][0],
        'attribution' => isset($block['field_uw_bq_quote_attribution'][0]) ? $block['field_uw_bq_quote_attribution'][0] : '',
      ];

      // Set the blockquote in variables.
      $variables->set('blockquote', $blockquote);
    }
  }
}
